#!/bin/sh
set -xe

# Configurable variables
PREFIX=${PREFIX:-$HOME/opt/ffmpeg}
MAKEOPTS=${MAKEOPTS:--j10}
RUSTFLAGS=${RUSTFLAGS:-"-C target-cpu=native"}
CMAKE_GENERATOR=${CMAKE_GENERATOR:-"Ninja"}
MSYSTEM=${MSYSTEM:-MINGW64}

export CMAKE_GENERATOR
export RUSTFLAGS
export MSYSTEM

# Install prerequisites
pacman -S --needed git perl vim mingw-w64-x86_64-toolchain	\
	mingw-w64-x86_64-cmake mingw-w64-x86_64-nasm		\
	mingw-w64-x86_64-yasm mingw-w64-x86_64-opus		\
	mingw-w64-x86_64-lame mingw-w64-x86_64-libvorbis	\
	mingw-w64-x86_64-libvpx mingw-w64-x86_64-fdk-aac	\
	mingw-w64-x86_64-x264-git mingw-w64-x86_64-x265		\
	mingw-w64-x86_64-libass mingw-w64-x86_64-libbluray	\
	mingw-w64-x86_64-openssl mingw-w64-x86_64-fontconfig	\
	mingw-w64-x86_64-freetype mingw-w64-x86_64-fribidi	\
	mingw-w64-x86_64-meson mingw-w64-x86_64-rust		\
	mingw-w64-x86_64-diffutils mingw-w64-x86_64-SDL2	\
	mingw-w64-x86_64-cargo-c mingw-w64-x86_64-ninja

# Create directory hierarchy
cd $HOME
mkdir -p $PREFIX/src

# Download everything
cd $PREFIX/src
git clone https://git.ffmpeg.org/ffmpeg.git || true
git clone https://aomedia.googlesource.com/aom || true
git clone https://github.com/xiph/rav1e || true
git clone https://github.com/AOMediaCodec/SVT-AV1 || true
git clone https://github.com/Netflix/vmaf || true
git clone https://code.videolan.org/videolan/dav1d || true
(cd ffmpeg; git pull)
(cd aom; git pull)
(cd rav1e; git pull)
(cd SVT-AV1; git pull)
(cd vmaf; git pull)
(cd dav1d; git pull)

# Build libaom
cd $PREFIX/src/aom
mkdir -p cmbuild
cd cmbuild
cmake -DCMAKE_INSTALL_PREFIX=$PREFIX -DBUILD_SHARED_LIBS=1 \
	-DCMAKE_BUILD_TYPE=Release -DENABLE_EXAMPLES=OFF -DENABLE_DOCS=off ..
cmake --build .
cmake --install .
cd $PREFIX
mkdir -p bin
mv lib/*.dll bin/

# Build librav1e
cd $PREFIX/src/rav1e
cargo cinstall --prefix=$PREFIX --release

# Build libSvtAv1Enc
cd $PREFIX/src/SVT-AV1/Build
cmake -DCMAKE_INSTALL_PREFIX=$PREFIX -DBUILD_SHARED_LIBS=1 -DBUILD_APPS=OFF \
	-DENABLE_AVX512=ON -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
cmake --install .
cd $PREFIX
mv lib/*.dll bin/

# Build libvmaf
cd $PREFIX/src/vmaf/libvmaf
meson --prefix=$PREFIX --libdir lib --buildtype release \
	-Denable_docs=false -Denable_avx512=true build
meson --reconfigure --prefix=$PREFIX --libdir lib --buildtype release \
	-Denable_docs=false -Denable_avx512=true build
ninja -C build install

mkdir -p $PREFIX/share
cd $PREFIX/src/vmaf
cp -R -P -p model $PREFIX/share

# Build libdav1d
cd $PREFIX/src/dav1d
meson --prefix=$PREFIX --libdir lib --buildtype release \
	-Denable_tools=false -Denable_avx512=true build
meson --reconfigure --prefix=$PREFIX --libdir lib --buildtype release \
	-Denable_tools=false -Denable_avx512=true build
ninja -C build install

# Build ffmpeg
cd $PREFIX/src/ffmpeg
PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig ./configure --prefix=$PREFIX	\
	--extra-ldflags="-L $PREFIX/lib" --disable-w32threads           \
	--enable-gpl --enable-nonfree --disable-doc			\
									\
	--enable-openssl --enable-libass --enable-libbluray		\
	--enable-libfreetype --enable-libfribidi			\
	--enable-libfontconfig						\
									\
	--enable-libfdk-aac --enable-libmp3lame	--enable-libopus	\
	--enable-libvorbis						\
									\
	--enable-libx264 --enable-libx265 --enable-libvpx		\
									\
	--enable-libvmaf --enable-librav1e --enable-libsvtav1		\
	--enable-libaom	 --enable-libdav1d
make $MAKEOPTS
make install
