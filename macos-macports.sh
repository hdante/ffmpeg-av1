#!/bin/sh
set -xe

# Configurable variables
PREFIX=${PREFIX:-$HOME/opt/ffmpeg}
MAKEOPTS=${MAKEOPTS:--j10}
RUSTFLAGS=${RUSTFLAGS:-"-C target-cpu=native"}
CMAKE_GENERATOR=${CMAKE_GENERATOR:-"Ninja"}
MACPORTS_PREFIX=${MACPORTS_PREFIX:-/opt/local}

export CMAKE_GENERATOR
export RUSTFLAGS

# Install prerequisites (requires privileges)
sudo port install git cmake nasm yasm libsdl2 libopus lame libvorbis	\
	libfdk-aac libvpx x264 x265 libass libbluray openssl fontconfig	\
	freetype fribidi meson diffutils rust cargo cargo-c ninja

# Create directory hierarchy
cd $HOME
mkdir -p $PREFIX/src

# Download everything
cd $PREFIX/src
git clone https://git.ffmpeg.org/ffmpeg.git || true
git clone https://aomedia.googlesource.com/aom || true
git clone https://github.com/xiph/rav1e || true
git clone https://github.com/AOMediaCodec/SVT-AV1 || true
git clone https://github.com/Netflix/vmaf || true
git clone https://code.videolan.org/videolan/dav1d || true
(cd ffmpeg; git pull)
(cd aom; git pull)
(cd rav1e; git pull)
(cd SVT-AV1; git pull)
(cd vmaf; git pull)
(cd dav1d; git pull)

# Build libaom
cd $PREFIX/src/aom
mkdir -p cmbuild
cd cmbuild
cmake -DCMAKE_INSTALL_PREFIX=$PREFIX -DBUILD_SHARED_LIBS=1 \
	-DCMAKE_BUILD_TYPE=Release -DENABLE_EXAMPLES=OFF -DENABLE_DOCS=off ..
cmake --build .
cmake --install .

# Build librav1e
cd $PREFIX/src/rav1e
cargo cinstall --prefix=$PREFIX --release

# Build libSvtAv1Enc
cd $PREFIX/src/SVT-AV1/Build
cmake -DCMAKE_INSTALL_PREFIX=$PREFIX -DBUILD_SHARED_LIBS=1 -DBUILD_APPS=OFF \
	-DENABLE_AVX512=ON -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
cmake --install .

# Build libvmaf
cd $PREFIX/src/vmaf/libvmaf
meson --prefix=$PREFIX --libdir lib --buildtype release \
	-Denable_docs=false -Denable_avx512=true build
meson --reconfigure --prefix=$PREFIX --libdir lib --buildtype release \
	-Denable_docs=false -Denable_avx512=true build
ninja -C build install

mkdir -p $PREFIX/share
cd $PREFIX/src/vmaf
cp -R -P -p model $PREFIX/share

# Build libdav1d
cd $PREFIX/src/dav1d
meson --prefix=$PREFIX --libdir lib --buildtype release \
	-Denable_tools=false -Denable_avx512=true build
meson --reconfigure --prefix=$PREFIX --libdir lib --buildtype release \
	-Denable_tools=false -Denable_avx512=true build
ninja -C build install

# Build ffmpeg
cd $PREFIX/src/ffmpeg
PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig ./configure --prefix=$PREFIX	\
	--extra-ldflags="-L $PREFIX/lib -L $MACPORTS_PREFIX/lib -Wl,-rpath,$PREFIX/lib -Wl,-rpath,$MACPORTS_PREFIX/lib" \
	--extra-cflags="-DTARGET_OS_OSX=1 -I $PREFIX/include"		\
	--extra-cxxflags="-DTARGET_OS_OSX=1 -I $PREFIX/include"		\
	--enable-gpl --enable-nonfree --disable-doc			\
									\
	--enable-openssl --enable-libass --enable-libbluray		\
	--enable-libfreetype --enable-libfribidi			\
	--enable-libfontconfig						\
									\
	--enable-libfdk-aac --enable-libmp3lame	--enable-libopus	\
	--enable-libvorbis						\
									\
	--enable-libx264 --enable-libx265 --enable-libvpx		\
									\
	--enable-libvmaf --enable-librav1e --enable-libsvtav1		\
	--enable-libaom	 --enable-libdav1d
make $MAKEOPTS
make install
